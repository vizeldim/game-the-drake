package thedrake.ui.fxml;

import thedrake.GameState;

public interface AppStageContext {
    void play();
    void startMenu();
    void endMenu(GameState gameState);
}
