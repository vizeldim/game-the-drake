package thedrake.ui.fxml;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

public class EndGameController implements Initializable {
    @FXML
    private Text result;

    private final AppStageContext appStageContext;

    public EndGameController(AppStageContext appStageContext) {
        this.appStageContext = appStageContext;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {}

    public void onPlay(){ appStageContext.play(); }

    public void onMenu(){ appStageContext.startMenu(); }

    public void setResult(String resultText) { result.setText(resultText); }
}
