package thedrake;

import java.io.PrintWriter;
import java.util.Optional;

public class GameState implements JSONSerializable{
	private final Board board;
	private final PlayingSide sideOnTurn;
	private final Army blueArmy;
	private final Army orangeArmy;
	private final GameResult result;
	
	public GameState(
			Board board, 
			Army blueArmy, 
			Army orangeArmy) {
		this(board, blueArmy, orangeArmy, PlayingSide.BLUE, GameResult.IN_PLAY);
	}
	
	public GameState(
			Board board, 
			Army blueArmy, 
			Army orangeArmy, 
			PlayingSide sideOnTurn, 
			GameResult result) {
		this.board = board;
		this.sideOnTurn = sideOnTurn;
		this.blueArmy = blueArmy;
		this.orangeArmy = orangeArmy;
		this.result = result;
	}
	
	public Board board() {
		return board;
	}

	public PlayingSide sideNotOnTurn() {
		return sideOnTurn == PlayingSide.BLUE ? PlayingSide.ORANGE : PlayingSide.BLUE;
	}

	public PlayingSide sideOnTurn() {
		return sideOnTurn;
	}
	
	public GameResult result() {
		return result;
	}
	
	public Army army(PlayingSide side) {
		if(side == PlayingSide.BLUE) {
			return blueArmy;
		}
		
		return orangeArmy;
	}
	
	public Army armyOnTurn() {
		return army(sideOnTurn);
	}
	
	public Army armyNotOnTurn() {
		if(sideOnTurn == PlayingSide.BLUE)
			return orangeArmy;
		
		return blueArmy;
	}
	
	public Tile tileAt(TilePos pos) {
		Optional<TroopTile> tile = blueArmy.boardTroops().at(pos);
		if (tile.isPresent()){
			return tile.get();
		}
		tile = orangeArmy.boardTroops().at(pos);
		if (tile.isPresent()){
			return tile.get();
		}

		return board.at(pos);
	}
	
	private boolean canStepFrom(TilePos origin) {
		return  !origin.equals(TilePos.OFF_BOARD) &&
				result.equals(GameResult.IN_PLAY) &&
				armyOnTurn().boardTroops().at(origin).isPresent() &&
				!blueArmy.boardTroops().isPlacingGuards() &&
				!orangeArmy.boardTroops().isPlacingGuards() &&
				blueArmy.boardTroops().isLeaderPlaced() &&
				orangeArmy.boardTroops().isLeaderPlaced();
	}

	private boolean canStepTo(TilePos target) {
		return !target.equals(TilePos.OFF_BOARD) &&
				result.equals(GameResult.IN_PLAY) &&
				board.at(target).canStepOn() &&
				armyOnTurn().boardTroops().at(target).isEmpty() &&
				armyNotOnTurn().boardTroops().at(target).isEmpty();
	}
	
	private boolean canCaptureOn(TilePos target) {
		return !target.equals(TilePos.OFF_BOARD) &&
				result.equals(GameResult.IN_PLAY) &&
				armyNotOnTurn().boardTroops().at(target).isPresent();
	}
	
	public boolean canStep(TilePos origin, TilePos target)  {
		return canStepFrom(origin) && canStepTo(target);
	}
	
	public boolean canCapture(TilePos origin, TilePos target)  {
		return canStepFrom(origin) && canCaptureOn(target);
	}
	
	public boolean canPlaceFromStack(TilePos target) {
		if (target.equals(TilePos.OFF_BOARD) ||
			!result.equals(GameResult.IN_PLAY) ||
			armyOnTurn().stack().isEmpty() ||
			!board.at(target).canStepOn() ||
			armyOnTurn().boardTroops().at(target).isPresent() ||
				armyNotOnTurn().boardTroops().at(target).isPresent()
		)
		{ return false; }

		if(!armyOnTurn().boardTroops().isLeaderPlaced()){
			if (sideOnTurn.equals(PlayingSide.BLUE)){
				return target.row() == 1;
			}
			return target.row() == board.dimension();
		}

		if (armyOnTurn().boardTroops().isPlacingGuards()){
			for (BoardPos el : board.positionFactory().pos(target.i(), target.j()).neighbours()){
				if (armyOnTurn().boardTroops().leaderPosition().equalsTo(el.i(),el.j())){
					return true;
				}
			}
			return false;
		}

		for (BoardPos el : board.positionFactory().pos(target.i(), target.j()).neighbours()){
			if (armyOnTurn().boardTroops().troopPositions().contains(el)){
				return true;
			}
		}
		return false;

	}
	
	public GameState stepOnly(BoardPos origin, BoardPos target) {		
		if(canStep(origin, target))		 
			return createNewGameState(
					armyNotOnTurn(),
					armyOnTurn().troopStep(origin, target), GameResult.IN_PLAY);
		
		throw new IllegalArgumentException();
	}
	
	public GameState stepAndCapture(BoardPos origin, BoardPos target) {
		if(canCapture(origin, target)) {
			Troop captured = armyNotOnTurn().boardTroops().at(target).get().troop();
			GameResult newResult = GameResult.IN_PLAY;
			
			if(armyNotOnTurn().boardTroops().leaderPosition().equals(target))
				newResult = GameResult.VICTORY;
			
			return createNewGameState(
					armyNotOnTurn().removeTroop(target), 
					armyOnTurn().troopStep(origin, target).capture(captured), newResult);
		}
		
		throw new IllegalArgumentException();
	}
	
	public GameState captureOnly(BoardPos origin, BoardPos target) {
		if(canCapture(origin, target)) {
			Troop captured = armyNotOnTurn().boardTroops().at(target).get().troop();
			GameResult newResult = GameResult.IN_PLAY;
			
			if(armyNotOnTurn().boardTroops().leaderPosition().equals(target))
				newResult = GameResult.VICTORY;
			
			return createNewGameState(
					armyNotOnTurn().removeTroop(target),
					armyOnTurn().troopFlip(origin).capture(captured), newResult);
		}
		
		throw new IllegalArgumentException();
	}
	
	public GameState placeFromStack(BoardPos target) {
		if(canPlaceFromStack(target)) {
			return createNewGameState(
					armyNotOnTurn(), 
					armyOnTurn().placeFromStack(target), 
					GameResult.IN_PLAY);
		}
		
		throw new IllegalArgumentException();
	}
	
	public GameState resign() {
		return createNewGameState(
				armyNotOnTurn(), 
				armyOnTurn(), 
				GameResult.VICTORY);
	}
	
	public GameState draw() {
		return createNewGameState(
				armyOnTurn(), 
				armyNotOnTurn(), 
				GameResult.DRAW);
	}
	
	private GameState createNewGameState(Army armyOnTurn, Army armyNotOnTurn, GameResult result) {
		if(armyOnTurn.side() == PlayingSide.BLUE) {
			return new GameState(board, armyOnTurn, armyNotOnTurn, PlayingSide.BLUE, result);
		}
		
		return new GameState(board, armyNotOnTurn, armyOnTurn, PlayingSide.ORANGE, result); 
	}

	@Override
	public void toJSON(PrintWriter writer) {
		writer.printf("{\"result\":");
		result().toJSON(writer);
		writer.printf(",\"board\":");
		board().toJSON(writer);
		writer.printf(",\"blueArmy\":");
		blueArmy.toJSON(writer);
		writer.printf(",\"orangeArmy\":");
		orangeArmy.toJSON(writer);
		writer.printf("%c",'}');
	}
}
