package thedrake;

import java.io.PrintWriter;

public class Board implements JSONSerializable{
    private final BoardTile[][] tiles; //final?? zalezi, co s tim budu delat potom
    private final int dimension;
    private final PositionFactory positionFactory;

    // Konstruktor. Vytvoří čtvercovou hrací desku zadaného rozměru, kde všechny dlaždice jsou prázdné, tedy BoardTile.EMPTY
    public Board(int dimension) {
        this.dimension = dimension;
        tiles = new BoardTile[dimension][dimension];
        positionFactory = new PositionFactory(dimension);

        for (int i = 0; i < dimension; ++i){
            for (int j = 0; j < dimension; ++j){
                tiles[i][j] = BoardTile.EMPTY;
            }
        }
    }

    // Rozměr hrací desky
    public int dimension() {
        return dimension;
    }

    // Vrací dlaždici na zvolené pozici.
    public BoardTile at(TilePos pos) {
        return tiles[pos.i()][pos.j()];
    }

    // Vytváří novou hrací desku s novými dlaždicemi. Všechny ostatní dlaždice zůstávají stejné
    public Board withTiles(TileAt ...ats) {
        Board newBoard = new Board(dimension);

        for (int i = 0; i < dimension; ++i){
            System.arraycopy(this.tiles[i], 0, newBoard.tiles[i], 0, dimension);
        }

        for (TileAt at : ats) {
            newBoard.tiles[at.pos.i()][at.pos.j()] = at.tile;
        }

        return newBoard;
    }

    // Vytvoří instanci PositionFactory pro výrobu pozic na tomto hracím plánu
    public PositionFactory positionFactory() {
        return positionFactory;
    }

    @Override
    public void toJSON(PrintWriter writer) {
        writer.printf("%c",'{');
        writer.printf("%s%d,","\"dimension\":", dimension);
        writer.printf("%s", "\"tiles\":[");
        for (int i = 0; i < dimension; ++i)
        {
            for (int j = 0; j < dimension; ++j)
            {
                tiles[j][i].toJSON(writer);
                if(i != dimension - 1 || j != dimension - 1)
                { writer.printf("%c", ','); }
            }
        }
        writer.printf("%c%c",']', '}');
    }

    public static class TileAt {
        public final BoardPos pos;
        public final BoardTile tile;

        public TileAt(BoardPos pos, BoardTile tile) {
            this.pos = pos;
            this.tile = tile;
        }
    }
}

